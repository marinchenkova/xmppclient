package xmppclient.util

import java.util.*

/**
 * @author Marinchenko V. A.
 */
class MessageBuffer {

    private val queue = ArrayDeque<String>()

    fun write(str: String?) {
        queue.add(str)
    }

    fun poll(): String? = queue.poll()
}