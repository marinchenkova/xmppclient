package xmppclient.util

import xmppclient.entity.Contact
import xmppclient.entity.Status

/**
 * @author Marinchenko V. A.
 */
interface XmppListener {

    fun onFriendStatusChanged(jid: String?, status: Status)
    fun onSubscribeRequest(jid: String?)
    fun onSubscribeAck(jid: String?)
    fun onMsgReceived(jid: String?, text: String?)
    fun onRosterChanged(contacts: HashMap<String?, Contact>)

}