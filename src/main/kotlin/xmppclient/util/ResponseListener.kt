package xmppclient.util

/**
 * @author Marinchenko V. A.
 */
interface ResponseListener {

    fun onReceive(msg: String?)

}