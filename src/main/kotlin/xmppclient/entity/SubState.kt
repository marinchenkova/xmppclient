package xmppclient.entity

/**
 * @author Marinchenko V. A.
 */
enum class SubState(val type: String) {
    REQUEST("subscribe"),
    ACK("subscribed"),
    UNKNOWN("unknown")
}