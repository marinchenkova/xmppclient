package xmppclient.entity

/**
 * @author Marinchenko V. A.
 */
enum class SubType(val type: String) {
    NONE("none"),
    TO("to"),
    FROM("from"),
    BOTH("both"),
    REMOVE("remove"),
    UNKNOWN("unknown")
}