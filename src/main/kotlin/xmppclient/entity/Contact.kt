package xmppclient.entity

/**
 * @author Marinchenko V. A.
 */
class Contact(
        var jid: String?,
        var sub: SubType?
) {
    var status: Status = Status.AWAY

    override fun toString() = "Contact: $jid, ${sub?.type}"
}

fun String.subType() = when (this) {
    SubType.TO.type -> SubType.TO
    SubType.FROM.type -> SubType.FROM
    SubType.BOTH.type -> SubType.BOTH
    SubType.NONE.type -> SubType.NONE
    else -> SubType.UNKNOWN
}
