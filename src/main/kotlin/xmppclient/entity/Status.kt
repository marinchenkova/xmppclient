package xmppclient.entity

import xmppclient.xml.stanzas.PRESENCE_AWAY
import xmppclient.xml.stanzas.PRESENCE_CHAT
import xmppclient.xml.stanzas.PRESENCE_UNA

/**
 * @author Marinchenko V. A.
 */
enum class Status(val state: String) {
    CHAT(PRESENCE_CHAT),
    AWAY(PRESENCE_AWAY),
    UNAVAILABLE(PRESENCE_UNA),
    UNKNOWN("unknown")
}