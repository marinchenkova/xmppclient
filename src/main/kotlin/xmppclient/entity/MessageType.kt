package xmppclient.entity

/**
 * @author Marinchenko V. A.
 */
enum class MessageType(val type: String) {
    CHAT("chat"),
    ERROR("error"),
    GROUPCHAT("groupchat"),
    HEADLINE("headline"),
    NORMAL("normal"),
}