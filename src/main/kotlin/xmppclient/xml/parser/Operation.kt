package xmppclient.xml.parser

/**
 * @author Marinchenko V. A.
 */
enum class Operation(val type: String) {
    INIT_STREAM("init_stream"),
    STREAM_FEATURES("stream_features"),
    AUTH("auth"),
    BIND("bind"),
    ROSTER_SET("roster_set"),
    ROSTER_RESULT("roster_result"),
    PRESENCE("presence"),
    MESSAGE("message"),
    UNKNOWN("unknown")
}