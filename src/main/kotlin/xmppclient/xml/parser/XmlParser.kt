package xmppclient.xml.parser

import xmppclient.xml.Response
import xmppclient.xml.parser.handlers.ElementHandler
import xmppclient.xml.parser.handlers.IqHandler
import xmppclient.xml.parser.handlers.StreamHandler
import xmppclient.xml.parser.handlers.MapHandler
import xmppclient.xml.stanzas.*
import xmppclient.xml.stanzas.base.*
import xmppclient.xml.stanzas.stream.Q_NAME_STREAM
import xmppclient.xml.stanzas.stream.Q_NAME_STREAM_FEATURES
import xmppclient.xml.stanzas.stream.Stream
import javax.xml.parsers.SAXParserFactory
import kotlin.reflect.KClass

/**
 * @author Marinchenko V. A.
 */
class XmlParser {

    private val parser = newSAXParser()

    private fun getMap(xml: String?): ArrayList<Stanza?> {
        val handler = MapHandler()
        val xmlnn = xml ?: ""
        try {
            parser.parse(xmlnn.byteInputStream(), handler)
        } catch (e: Exception) {
        }
        return handler.map
    }

    fun getResponse(xml: String?): Response {
        val map = getMap(xml)

        if (map.has(Q_NAME_STREAM)) return Response(Operation.INIT_STREAM, map)

        if (map.has(RESULT_SUCCESS) || map.has(RESULT_FAILURE)) return Response(Operation.AUTH, map)

        if (map.has(Q_NAME_PRESENCE)) return Response(Operation.PRESENCE, map)

        if (map.has(Q_NAME_MSG)) return Response(Operation.MESSAGE, map)

        if (map.has(Q_NAME_IQ)) {
            val iq = map.extract<Iq>()

            if (iq?.bind != null) return Response(Operation.BIND, map)

            when (iq?.type) {
                VAL_TYPE_SET -> return Response(Operation.ROSTER_SET, map)
                VAL_TYPE_RESULT -> return Response(Operation.ROSTER_RESULT, map)
            }
        }

        return Response(Operation.UNKNOWN, map)
    }

    companion object {
        @JvmStatic
        private fun newSAXParser() = SAXParserFactory
                .newInstance()
                .apply { isNamespaceAware = true }
                .newSAXParser()
    }

}


fun ArrayList<Stanza?>.has(name: String): Boolean {
    this.forEach {
        when (name) {
            Q_NAME_STREAM -> if (it is Stream) return true
            RESULT_FAILURE, RESULT_SUCCESS -> if (it is Result) return true
            Q_NAME_PRESENCE -> if (it is Presence) return true
            Q_NAME_MSG -> if (it is Message) return true
            Q_NAME_IQ -> if (it is Iq) return true
        }
    }
    return false
}

inline fun <reified S: Stanza> ArrayList<Stanza?>.extract(): S? {
    this.forEach { if (it is S) return it }
    return null
}