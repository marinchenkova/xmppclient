package xmppclient.xml.parser.handlers

import org.xml.sax.Attributes
import xmppclient.xml.stanzas.*
import xmppclient.xml.stanzas.base.*
import xmppclient.xml.stanzas.Bind
import xmppclient.xml.stanzas.Q_NAME_BIND

/**
 * @author Marinchenko V. A.
 */
class IqHandler : ElementHandler<Iq>(elements) {

    override var stanza: Iq? = null

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        super.startElement(uri, localName, qName, attributes)

        if (tags[Q_NAME_IQ] == true) {

            if (qName == Q_NAME_IQ) stanza = Iq(
                    attributes?.getValue(ATTR_ID),
                    attributes?.getValue(ATTR_TYPE),
                    attributes?.getValue(ATTR_FROM),
                    attributes?.getValue(ATTR_TO)
            )

            if (tags[Q_NAME_QUERY] == true) {

                when (qName) {
                    Q_NAME_QUERY -> stanza?.query = Query(uri)
                    TAG_USERNAME -> stanza?.query?.tags?.put(TAG_USERNAME, Tag(TAG_USERNAME))
                    TAG_EMAIL -> stanza?.query?.tags?.put(TAG_EMAIL, Tag(TAG_EMAIL))
                    TAG_PASSWORD -> stanza?.query?.tags?.put(TAG_PASSWORD, Tag(TAG_PASSWORD))
                    Q_NAME_ITEM -> stanza?.query?.tags?.put(Q_NAME_ITEM, Item(
                            attributes?.getValue(TAG_JID),
                            attributes?.getValue(ATTR_NAME),
                            attributes?.getValue(ATTR_SUB),
                            attributes?.getValue(ATTR_ASK)
                    ))
                }

            }

            if (tags[Q_NAME_BIND] == true) {

                when (qName) {
                    Q_NAME_BIND -> stanza?.bind = Bind()
                    TAG_RESOURCE -> stanza?.bind?.tag = Tag(TAG_RESOURCE)
                    TAG_JID -> stanza?.bind?.tag = Tag(TAG_JID)
                }

            }


            if (tags[Q_NAME_ERROR] == true) {

                when (qName) {
                    Q_NAME_ERROR -> stanza?.error = Error(
                            attributes?.getValue(ATTR_CODE),
                            attributes?.getValue(ATTR_TYPE)
                    )
                    else -> stanza?.error?.what = qName
                }


            }

        }

    }

    override fun characters(ch: CharArray?, start: Int, length: Int) {
        val data = String(ch ?: charArrayOf(), start, length).trim()

        if (tags[Q_NAME_IQ] == true) {

            if (tags[Q_NAME_ERROR] == true) {
                stanza?.error?.data = data
            }

            if (tags[Q_NAME_QUERY] == true) {

                if (tags[TAG_USERNAME] == true) {
                    (stanza?.query?.tags?.get(TAG_USERNAME) as Tag).withData(data)
                }

                if (tags[TAG_EMAIL] == true) {
                    (stanza?.query?.tags?.get(TAG_EMAIL) as Tag).withData(data)
                }

                if (tags[TAG_PASSWORD] == true) {
                    (stanza?.query?.tags?.get(TAG_PASSWORD) as Tag).withData(data)
                }

            }

            if (tags[Q_NAME_BIND] == true) {
                if (tags[TAG_RESOURCE] == true || tags[TAG_JID] == true) {
                    stanza?.bind?.tag?.withData(data)
                }
            }
        }
    }


    companion object {
        private val elements = listOf(
                Q_NAME_IQ,
                Q_NAME_QUERY,
                Q_NAME_ERROR,
                Q_NAME_BIND,
                TAG_RESOURCE,
                TAG_JID,
                TAG_EMAIL,
                TAG_USERNAME,
                TAG_PASSWORD,
                Q_NAME_ITEM
        )
    }
}