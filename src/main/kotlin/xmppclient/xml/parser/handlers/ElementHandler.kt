package xmppclient.xml.parser.handlers

import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler
import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */
abstract class ElementHandler<S : Stanza>(elements: List<String>) : DefaultHandler() {

    abstract var stanza: S?
    protected val tags: MutableMap<String?, Boolean> = elements.associate { it to false }.toMutableMap()
    protected var prevStart: String? = null
    private var currStart: String? = null

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        if (tags.containsKey(qName)) {
            tags[qName] = true
        }
        prevStart = currStart
        currStart = qName
    }

    override fun endElement(uri: String?, localName: String?, qName: String?) {
        if (tags.containsKey(qName)) tags[qName] = false
    }
}