package xmppclient.xml.parser.handlers

import org.xml.sax.Attributes
import xmppclient.xml.stanzas.base.*
import xmppclient.xml.stanzas.stream.*

/**
 * @author Marinchenko V. A.
 */
class StreamHandler : ElementHandler<Stream>(elements) {

    override var stanza: Stream? = null

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        super.startElement(uri, localName, qName, attributes)

        if (tags[Q_NAME_STREAM] == true) {

            if (qName == Q_NAME_STREAM) {
                stanza = Stream(
                        attributes?.getValue(ATTR_TO),
                        attributes?.getValue(ATTR_FROM),
                        attributes?.getValue(ATTR_ID),
                        attributes?.getValue(ATTR_XML_LANG),
                        attributes?.getValue(ATTR_VERSION)
                )
            }

            if (tags[Q_NAME_STREAM_FEATURES] == true) {

                when (qName) {
                    Q_NAME_CAPS, Q_NAME_CAPS_SHORT -> stanza?.features?.caps = Caps(
                            attributes?.getValue(ATTR_HASH),
                            attributes?.getValue(ATTR_NODE),
                            attributes?.getValue(ATTR_VER)
                    )
                    Q_NAME_REGISTER -> stanza?.features?.register = FeatureState.CAN
                    Q_NAME_STARTTLS -> stanza?.features?.starttls = FeatureState.CAN
                    Q_NAME_PUSH -> stanza?.features?.push = FeatureState.CAN
                    Q_NAME_REBIND -> stanza?.features?.rebind = FeatureState.CAN
                    Q_NAME_ACK -> stanza?.features?.ack = FeatureState.CAN
                }

            }
        }
    }

    override fun endElement(uri: String?, localName: String?, qName: String?) {
        super.endElement(uri, localName, qName)

        if (qName == Q_NAME_REQUIRED) {
            when (prevStart) {
                Q_NAME_REGISTER -> stanza?.features?.register = FeatureState.REQUIRED
                Q_NAME_STARTTLS -> stanza?.features?.starttls = FeatureState.REQUIRED
                Q_NAME_PUSH -> stanza?.features?.push = FeatureState.REQUIRED
                Q_NAME_REBIND -> stanza?.features?.rebind = FeatureState.REQUIRED
                Q_NAME_ACK -> stanza?.features?.ack = FeatureState.REQUIRED
            }
        }
    }

    override fun characters(ch: CharArray?, start: Int, length: Int) {
        val data = String(ch ?: charArrayOf(), start, length).trim()

        if (tags[Q_NAME_STREAM] == true) {

            if (tags[Q_NAME_STREAM_FEATURES] == true && data.isNotEmpty()) {

                if (tags[Q_NAME_COMPRESSION] == true && tags[Q_NAME_METHOD] == true) {
                    stanza?.features?.compress?.add(data.compressionType())
                }

                if (tags[Q_NAME_MECHANISMS] == true && tags[Q_NAME_MECHANISM] == true) {
                    stanza?.features?.auth?.add(data.authMech())
                }

            }
        }
    }

    companion object {
        private val elements = listOf(
                Q_NAME_STREAM,
                Q_NAME_STREAM_FEATURES,
                Q_NAME_COMPRESSION,
                Q_NAME_MECHANISMS,
                Q_NAME_MECHANISM,
                Q_NAME_METHOD
        )
    }
}