package xmppclient.xml.parser.handlers

import org.xml.sax.Attributes
import xmppclient.xml.stanzas.RESULT_FAILURE
import xmppclient.xml.stanzas.RESULT_SUCCESS
import xmppclient.xml.stanzas.*
import xmppclient.xml.stanzas.base.*
import xmppclient.xml.stanzas.stream.*

/**
 * @author Marinchenko V. A.
 */
class MapHandler : ElementHandler<Stanza>(elements) {

    override var stanza: Stanza? = null
    val map = ArrayList<Stanza?>()

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        super.startElement(uri, localName, qName, attributes)

        if (tags[RESULT_SUCCESS] == true) {
            stanza = Result(true, null)
        }

        if (tags[RESULT_FAILURE] == true) {
            stanza = Result(false, null)
        }

        if (tags[Q_NAME_PRESENCE] == true) {

            if (qName == Q_NAME_PRESENCE) stanza = Presence(
                    attributes?.getValue(ATTR_FROM),
                    attributes?.getValue(ATTR_TO),
                    attributes?.getValue(ATTR_TYPE)
            )

            if (tags[Q_NAME_SHOW] == true) {
                (stanza as Presence).withShow(Tag(Q_NAME_SHOW))
            }
        }

        if (tags[Q_NAME_MSG] == true) {

            if (qName == Q_NAME_MSG) stanza = Message(
                    attributes?.getValue(ATTR_TYPE),
                    attributes?.getValue(ATTR_TO),
                    attributes?.getValue(ATTR_ID),
                    attributes?.getValue(ATTR_FROM)
            )
        }

        if (tags[Q_NAME_STREAM] == true) {

            if (qName == Q_NAME_STREAM) {
                stanza = Stream(
                        attributes?.getValue(ATTR_TO),
                        attributes?.getValue(ATTR_FROM),
                        attributes?.getValue(ATTR_ID),
                        attributes?.getValue(ATTR_XML_LANG),
                        attributes?.getValue(ATTR_VERSION)
                )
            }

            if (tags[Q_NAME_STREAM_FEATURES] == true) {
                (stanza as Stream).features = StreamFeatures()
                when (qName) {
                    Q_NAME_CAPS, Q_NAME_CAPS_SHORT -> (stanza as Stream).features?.caps = Caps(
                            attributes?.getValue(ATTR_HASH),
                            attributes?.getValue(ATTR_NODE),
                            attributes?.getValue(ATTR_VER)
                    )
                    Q_NAME_REGISTER -> (stanza as Stream).features?.register = FeatureState.CAN
                    Q_NAME_STARTTLS -> (stanza as Stream).features?.starttls = FeatureState.CAN
                    Q_NAME_PUSH -> (stanza as Stream).features?.push = FeatureState.CAN
                    Q_NAME_REBIND -> (stanza as Stream).features?.rebind = FeatureState.CAN
                    Q_NAME_ACK -> (stanza as Stream).features?.ack = FeatureState.CAN
                }

            }
        }

        if (tags[Q_NAME_IQ] == true) {

            if (qName == Q_NAME_IQ) stanza = Iq(
                    attributes?.getValue(ATTR_ID),
                    attributes?.getValue(ATTR_TYPE),
                    attributes?.getValue(ATTR_FROM),
                    attributes?.getValue(ATTR_TO)
            )

            if (tags[Q_NAME_QUERY] == true) {

                when (qName) {
                    Q_NAME_QUERY -> (stanza as Iq).query = Query(uri)
                    TAG_USERNAME -> (stanza as Iq).query?.tags?.put(TAG_USERNAME, Tag(TAG_USERNAME))
                    TAG_EMAIL -> (stanza as Iq).query?.tags?.put(TAG_EMAIL, Tag(TAG_EMAIL))
                    TAG_PASSWORD -> (stanza as Iq).query?.tags?.put(TAG_PASSWORD, Tag(TAG_PASSWORD))
                    Q_NAME_ITEM -> (stanza as Iq).query?.items?.add(Item(
                            attributes?.getValue(TAG_JID),
                            attributes?.getValue(ATTR_NAME),
                            attributes?.getValue(ATTR_SUB),
                            attributes?.getValue(ATTR_ASK)
                    ))
                }

            }

            if (tags[Q_NAME_BIND] == true) {

                when (qName) {
                    Q_NAME_BIND -> (stanza as Iq).bind = Bind()
                    TAG_RESOURCE -> (stanza as Iq).bind?.tag = Tag(TAG_RESOURCE)
                    TAG_JID -> (stanza as Iq).bind?.tag = Tag(TAG_JID)
                }

            }


            if (tags[Q_NAME_ERROR] == true) {

                when (qName) {
                    Q_NAME_ERROR -> (stanza as Iq).error = Error(
                            attributes?.getValue(ATTR_CODE),
                            attributes?.getValue(ATTR_TYPE)
                    )
                    else -> (stanza as Iq).error?.what = qName
                }


            }

        }

        map.add(stanza)
    }

    override fun endElement(uri: String?, localName: String?, qName: String?) {
        super.endElement(uri, localName, qName)

        if (qName == Q_NAME_REQUIRED) {
            when (prevStart) {
                Q_NAME_REGISTER -> (stanza as Stream).features?.register = FeatureState.REQUIRED
                Q_NAME_STARTTLS -> (stanza as Stream).features?.starttls = FeatureState.REQUIRED
                Q_NAME_PUSH -> (stanza as Stream).features?.push = FeatureState.REQUIRED
                Q_NAME_REBIND -> (stanza as Stream).features?.rebind = FeatureState.REQUIRED
                Q_NAME_ACK -> (stanza as Stream).features?.ack = FeatureState.REQUIRED
            }
        }
    }

    override fun characters(ch: CharArray?, start: Int, length: Int) {
        val data = String(ch ?: charArrayOf(), start, length).trim()

        if (tags[Q_NAME_STREAM] == true) {

            if (tags[Q_NAME_STREAM_FEATURES] == true && data.isNotEmpty()) {

                if (tags[Q_NAME_COMPRESSION] == true && tags[Q_NAME_METHOD] == true) {
                    (stanza as Stream).features?.compress?.add(data.compressionType())
                }

                if (tags[Q_NAME_MECHANISMS] == true && tags[Q_NAME_MECHANISM] == true) {
                    (stanza as Stream).features?.auth?.add(data.authMech())
                }

            }
        }

        if (tags[Q_NAME_MSG] == true) {

            if (tags[TAG_BODY] == true) {
                (stanza as Message).withText(data)
            }
        }

        if (tags[Q_NAME_PRESENCE] == true) {

            if (tags[Q_NAME_SHOW] == true) {
                (stanza as Presence).show?.withData(data)
            }

        }

        if (tags[Q_NAME_IQ] == true) {

            if (tags[Q_NAME_ERROR] == true) {
                (stanza as Iq).error?.data = data
            }

            if (tags[Q_NAME_QUERY] == true) {

                if (tags[TAG_USERNAME] == true) {
                    ((stanza as Iq).query?.tags?.get(TAG_USERNAME) as Tag).withData(data)
                }

                if (tags[TAG_EMAIL] == true) {
                    ((stanza as Iq).query?.tags?.get(TAG_EMAIL) as Tag).withData(data)
                }

                if (tags[TAG_PASSWORD] == true) {
                    ((stanza as Iq).query?.tags?.get(TAG_PASSWORD) as Tag).withData(data)
                }

            }

            if (tags[Q_NAME_BIND] == true) {
                if (tags[TAG_RESOURCE] == true || tags[TAG_JID] == true) {
                    (stanza as Iq).bind?.tag?.withData(data)
                }
            }
        }
    }


    companion object {
        private val elements = listOf(
                Q_NAME_IQ,
                Q_NAME_QUERY,
                Q_NAME_ERROR,
                Q_NAME_BIND,
                TAG_RESOURCE,
                TAG_JID,
                TAG_EMAIL,
                TAG_USERNAME,
                TAG_PASSWORD,
                TAG_BODY,
                Q_NAME_ITEM,
                Q_NAME_STREAM,
                Q_NAME_STREAM_FEATURES,
                Q_NAME_COMPRESSION,
                Q_NAME_MECHANISMS,
                Q_NAME_MECHANISM,
                Q_NAME_METHOD,
                Q_NAME_MSG,
                RESULT_SUCCESS,
                RESULT_FAILURE,
                Q_NAME_PRESENCE,
                Q_NAME_SHOW
        )
    }
}