package xmppclient.xml.parser.handlers

import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler
import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */
abstract class XmlHandler : DefaultHandler() {

    val stanzas = ArrayList<Stanza?>()

    private fun clear() {
        stanzas.clear()
    }

    override fun startDocument() {
        clear()
    }

}