package xmppclient.xml

import xmppclient.xml.parser.Operation
import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */
class Response(val operation: Operation, val map: ArrayList<Stanza?>)