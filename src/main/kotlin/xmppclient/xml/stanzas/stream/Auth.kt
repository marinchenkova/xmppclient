package xmppclient.xml.stanzas.stream

import xmppclient.xml.stanzas.base.ATTR_NS
import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_AUTH = "auth"

const val ATTR_MECHANISM = "mechanism"

const val VAL_NS_AUTH = "urn:ietf:params:xml:ns:xmpp-sasl"

class Auth(
        val mechanism: AuthMechanism,
        val data: String
) : Stanza {

    override fun xml() = """
        <$Q_NAME_AUTH $ATTR_NS='$VAL_NS_AUTH' $ATTR_MECHANISM='$mechanism'>$data</$Q_NAME_AUTH>
    """.trimIndent()

    override fun toString() = """
        Auth: $mechanism [$data]
    """.trimIndent()
}