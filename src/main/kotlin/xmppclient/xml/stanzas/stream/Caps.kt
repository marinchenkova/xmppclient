package xmppclient.xml.stanzas.stream

import xmppclient.xml.stanzas.base.ATTR_NS
import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_CAPS = "caps"
const val Q_NAME_CAPS_SHORT = "c"

const val ATTR_HASH = "hash"
const val ATTR_NODE = "node"
const val ATTR_VER = "ver"

const val VAL_NS_CAPS = "http://jabber.org/protocol/caps"

class Caps(
     val hash: String?,
     val node: String?,
     val ver: String?
) : Stanza {

    override fun xml() = """
        <$Q_NAME_CAPS
            $ATTR_NS='$VAL_NS_CAPS'
            ${if (hash != null) "$ATTR_HASH='$hash'" else ""}
            ${if (node != null) "$ATTR_NODE='$node'" else ""}
            ${if (ver != null) "$ATTR_VER='$ver'" else ""}
        >
    """.trimIndent()


    override fun toString() = """
        Caps: hash=$hash, node=$node, ver=$ver
    """.trimIndent()
}