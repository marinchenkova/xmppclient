package xmppclient.xml.stanzas.stream

/**
 * @author Marinchenko V. A.
 */
enum class FeatureState(state: Int) {
    NO(0),
    CAN(0),
    REQUIRED(0)
}