package xmppclient.xml.stanzas.stream

import xmppclient.xml.stanzas.base.*

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_STREAM = "stream:stream"

const val ATTR_VERSION = "version"
const val ATTR_NS_STREAM = "$ATTR_NS:stream"
const val ATTR_NS_DB = "$ATTR_NS:db"


const val VAL_NS_STREAM = "http://etherx.jabber.org/streams"
const val VAL_NS_CLIENT = "jabber:client"
const val VAL_NS_DB = "jabber:server:dialback"

class Stream(
        val to: String?,
        val from: String?,
        val id: String?,
        val xmlLang: String?,
        val version: String?
) : Stanza {

    var features: StreamFeatures? = null

    override fun xml() =
        "<?xml version='1.0'?>\n" +
        "<$Q_NAME_STREAM\n" +
            "$ATTR_NS_STREAM='$VAL_NS_STREAM'\n" +
            "$ATTR_NS='$VAL_NS_CLIENT'\n" +
            //"$ATTR_NS_DB='$VAL_NS_DB'\n" +
            "$ATTR_VERSION='$version'\n" +
            "$ATTR_XML_LANG='$xmlLang'\n" +
                (if (to != null) "$ATTR_TO='$to'\n" else "") +
                (if (from != null) "$ATTR_FROM='$from'\n" else "") +
                (if (id != null) "$ATTR_ID='$id'\n" else "") +
                ">"

    override fun toString() = """
        Stream:
            to=$to, from=$from, id=$id, xml:lang=$xmlLang, version=$version
            auth: ${features?.auth?.joinToString { it.name }}
            compress: ${features?.compress?.joinToString { it.name }}
            ${features?.caps}
            register: ${features?.register}
            starttls: ${features?.starttls}
            push: ${features?.push}
            rebind: ${features?.rebind}
            ack: ${features?.ack}
    """.trimIndent()


    companion object {
        @JvmStatic
        fun to(server: String?, id: String?, version: String, xmlLang: String?) =
                Stream(server, null, id, xmlLang, version).xml()
    }
}