package xmppclient.xml.stanzas.stream

/**
 * @author Marinchenko V. A.
 */

enum class CompressionType(type: String) {
    ZLIB("zlib"),
    UNKNOWN("unknown")
}

fun String.compressionType() = when (this) {
    CompressionType.ZLIB.name -> CompressionType.ZLIB
    else -> CompressionType.UNKNOWN
}