package xmppclient.xml.stanzas.stream

import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_STREAM_FEATURES = "stream:features"
const val Q_NAME_REGISTER = "register"
const val Q_NAME_STARTTLS = "starttls"
const val Q_NAME_PUSH = "push"
const val Q_NAME_REBIND = "rebind"
const val Q_NAME_ACK = "ack"
const val Q_NAME_MECHANISMS = "mechanisms"
const val Q_NAME_MECHANISM = "mechanism"
const val Q_NAME_COMPRESSION = "compression"
const val Q_NAME_METHOD = "method"
const val Q_NAME_REQUIRED = "required"

class StreamFeatures {

    var auth = ArrayList<AuthMechanism>()
    var compress = ArrayList<CompressionType>()
    var caps: Caps? = null
    var register = FeatureState.NO
    var starttls = FeatureState.NO
    var push = FeatureState.NO
    var rebind = FeatureState.NO
    var ack = FeatureState.NO

}