package xmppclient.xml.stanzas.stream

/**
 * @author Marinchenko V. A.
 */
enum class AuthMechanism(type: String) {
    DIGEST_MD5("DIGEST-MD5"),
    PLAIN("PLAIN"),
    X_OAUTH2("X-OAUTH2"),
    SCRAM_SHA_1("SCRAM-SHA-1"),
    UNKNOWN("unknown")
}

fun String.authMech() = when (this) {
    AuthMechanism.DIGEST_MD5.name -> AuthMechanism.DIGEST_MD5
    AuthMechanism.PLAIN.name -> AuthMechanism.PLAIN
    AuthMechanism.X_OAUTH2.name -> AuthMechanism.X_OAUTH2
    AuthMechanism.SCRAM_SHA_1.name -> AuthMechanism.SCRAM_SHA_1
    else -> AuthMechanism.UNKNOWN
}