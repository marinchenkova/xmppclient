package xmppclient.xml.stanzas

import xmppclient.entity.Contact
import xmppclient.entity.SubState
import xmppclient.entity.SubType
import xmppclient.entity.subType
import xmppclient.xml.stanzas.base.ATTR_FROM
import xmppclient.xml.stanzas.base.ATTR_ID
import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.Tag

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_ITEM = "item"

const val VAL_NS_ROSTER = "jabber:iq:roster"
const val VAL_SUB_NONE = "none"
const val VAL_SUB_RM = "remove"

const val ATTR_SUB = "subscription"
const val ATTR_NAME = "name"
const val ATTR_ASK = "ask"

const val TAG_GROUP = "group"

class Item(
        val jid: String?,
        val name: String?,
        val sub: String?,
        val ask: String?
) : Stanza {

    var group: Tag? = null

    fun withGroup(groupName: String): Item {
        group = Tag(TAG_GROUP).withData(groupName)
        return this
    }

    fun groupName() = group?.data

    fun getSubType() = when (sub) {
        SubType.NONE.type -> SubType.NONE
        SubType.FROM.type -> SubType.FROM
        SubType.TO.type -> SubType.TO
        SubType.BOTH.type -> SubType.BOTH
        SubType.REMOVE.type -> SubType.REMOVE
        else -> SubType.UNKNOWN
    }

    override fun xml() =
            "<$Q_NAME_ITEM " +
                    (if (jid != null) "$TAG_JID='$jid' " else "") +
                    (if (name != null) "${ATTR_NAME}='$name' " else "") +
                    (if (sub != null) "${ATTR_SUB}='$sub' " else "") +
                    (if (ask != null) "${ATTR_ASK}='$ask' " else "") + ">\n" +
                    (if (group != null) "${group?.xml()}\n" else "") +
                    "</$Q_NAME_ITEM>"

    override fun toString() =
            "Item: jid=$jid, name=$name, sub=$sub\n" +
                    "$group"

}

fun ArrayList<Item>.toContacts(): HashMap<String?, Contact> {
    val map = HashMap<String?, Contact>()
    this.forEach { map.put(it.jid, Contact(it.jid, it.sub?.subType())) }
    return map
}