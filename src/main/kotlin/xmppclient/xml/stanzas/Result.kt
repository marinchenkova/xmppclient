package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.Stanza

/**
 * @author Marinchenko V. A.
 */

const val RESULT_SUCCESS = "success"
const val RESULT_FAILURE = "failure"

class Result(
        success: Boolean?,
        val msg: String? = null
) : Stanza {

    val success: Boolean = success ?: false

    override fun xml() = ""

    override fun toString() = "Result: " +
            if (success) RESULT_SUCCESS
            else "${RESULT_FAILURE} $msg"
}