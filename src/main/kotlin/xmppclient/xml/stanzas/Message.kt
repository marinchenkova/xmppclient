package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.*

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_MSG = "message"
const val TAG_BODY = "body"

const val VAL_MSG_CHAT = "chat"
const val VAL_MSG_ERROR = "error"
const val VAL_MSG_GROUP = "group"
const val VAL_MSG_HEAD = "head"
const val VAL_MSG_NORMAL = "normal"

class Message(
        val type: String?,
        val to: String?,
        val id: String?,
        val from: String?
) : Stanza {

    var body: Tag? = null

    fun withText(msg: String?): Message {
        body = Tag(TAG_BODY).withData(msg)
        return this
    }

    fun getText() = body?.data

    override fun xml() =
            "<$Q_NAME_MSG " +
                    (if (type != null) "$ATTR_TYPE='$type' " else "") +
                    (if (to != null) "$ATTR_TO='$to' " else "") +
                    (if (from != null) "$ATTR_FROM='$from' " else "") +
                    (if (id != null) "$ATTR_ID='$id' " else "") + ">" +
                    (if (body != null) "${body?.xml()}\n" else "") +
                    "</$Q_NAME_MSG>"

    override fun toString() =
            "Message: to=$to, from=$from, type=$type, id=$id\n" +
                    "$body"

    companion object {
        @JvmStatic
        fun write(to: String?, from: String?, text: String?) = Message(
                VAL_MSG_CHAT,
                to,
                null,
                from
        ).withText(text).xml()

    }

}