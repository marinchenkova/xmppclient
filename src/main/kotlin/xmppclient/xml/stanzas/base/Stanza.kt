package xmppclient.xml.stanzas.base

/**
 * @author Marinchenko V. A.
 */

const val ATTR_TO = "to"
const val ATTR_FROM = "from"
const val ATTR_ID = "id"
const val ATTR_XML_LANG = "xml:lang"
const val ATTR_NS = "xmlns"

const val VAL_XML_LANG_EN = "en"
const val VAL_XML_LANG_RU = "ru"

interface Stanza {

    fun xml(): String

}