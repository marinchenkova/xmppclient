package xmppclient.xml.stanzas.base

/**
 * @author Marinchenko V. A.
 */
open class Tag(
        val name: String
) : Stanza {

    var data: String? = null

    fun withData(data: String?): Tag {
        this.data = data
        return this
    }

    override fun xml() = "<$name>${if (data != null) data else ""}</$name>"

    override fun toString() = "$name: data=[$data]"

}