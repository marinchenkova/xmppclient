package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.ATTR_NS
import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.Tag

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_BIND = "bind"
const val TAG_RESOURCE = "resource"
const val TAG_JID = "jid"

const val VAL_NS_BIND = "urn:ietf:params:xml:ns:xmpp-bind"
const val VAL_RES = "default"

class Bind : Stanza {

    var tag: Tag? = null

    fun withTag(tag: Tag): Bind {
        this.tag = tag
        return this
    }

    override fun xml() =
            "<${Q_NAME_BIND} $ATTR_NS='${VAL_NS_BIND}'>" +
                    (if (tag != null) "\n${tag?.xml()}\n" else "") +
                    "</${Q_NAME_BIND}>"

    override fun toString() =
            "Bind: tag: $tag"

    fun getJid() = if (tag?.data != null && tag?.name == TAG_JID) tag?.data
    else null

}