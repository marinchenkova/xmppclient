package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.*

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_IQ = "iq"

const val ATTR_TYPE = "type"

const val VAL_ID_REG1 = "reg1"
const val VAL_ID_BND1 = "bnd1"
const val VAL_ID_ADD = "add1"
const val VAL_ID_RM = "rm1"
const val VAL_ID_GET = "get1"

const val VAL_TYPE_GET = "get"
const val VAL_TYPE_SET = "set"
const val VAL_TYPE_RESULT = "result"


class Iq(
        val id: String?,
        val type: String?,
        val from: String?,
        val to: String?
): Stanza {

    var query: Query? = null
    var bind: Bind? = null
    var error: Error? = null

    fun succeed() = error == null

    fun errorText(): String? = if (error == null) null
    else error?.data ?: "${error?.type} ${error?.code} ${error?.what}"

    override fun xml() =
        "<$Q_NAME_IQ " +
                (if (type != null) "$ATTR_TYPE='$type' " else "") +
                (if (id != null) "$ATTR_ID='$id' " else "") +
                (if (from != null) "$ATTR_FROM='$from'" else "") +
                (if (to != null) "$ATTR_TO='$to'" else "") + ">\n" +
                (if (query != null) "${query?.xml()}\n" else "") +
                (if (error != null) "${error?.xml()}\n" else "") +
                (if (bind != null) "${bind?.xml()}\n" else "") +
                "</$Q_NAME_IQ>"


    override fun toString() =
            "Iq: id=$id, type=$type, from=$from\n" +
                    "$bind\n" +
                    "$query\n" +
                    "$error"

    companion object {
        @JvmStatic
        fun rosterGet(from: String?) = Iq(
                VAL_ID_GET,
                VAL_TYPE_GET,
                from,
                null
        ).apply {
            query = Query(VAL_NS_ROSTER)
        }.xml()


        @JvmStatic
        fun rosterAdd(jid: String, group: String) = Iq(
                VAL_ID_ADD,
                VAL_TYPE_SET,
                null,
                null
        ).apply {
            query = Query(VAL_NS_ROSTER).apply {
                tags.put(Q_NAME_ITEM,
                        Item(jid, null, VAL_SUB_NONE, null).withGroup(group)
                )
            }
        }.xml()

        @JvmStatic
        fun rosterRemove(from: String?, jid: String?) = Iq(
                VAL_ID_RM,
                VAL_TYPE_SET,
                from,
                null
        ).apply {
            query = Query(VAL_NS_ROSTER).apply {
                tags.put(
                        Q_NAME_ITEM,
                        Item(jid, null, VAL_SUB_RM, null)
                )
            }
        }.xml()

        @JvmStatic
        fun ackPush(id: String?) = Iq(
                id,
                VAL_TYPE_RESULT,
                null,
                null
        ).xml()

        @JvmStatic
        fun bind(resource: String = VAL_RES) = Iq(
                VAL_ID_BND1,
                VAL_TYPE_SET,
                null,
                null
        ).apply {
            bind = Bind().withTag(Tag(TAG_RESOURCE).withData(resource))
        }.xml()
    }
}