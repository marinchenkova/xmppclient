package xmppclient.xml.stanzas

import xmppclient.entity.Status
import xmppclient.entity.SubState
import xmppclient.entity.SubType
import xmppclient.xml.stanzas.base.ATTR_FROM
import xmppclient.xml.stanzas.base.ATTR_TO
import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.Tag

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_PRESENCE = "presence"
const val Q_NAME_SHOW = "show"

const val VAL_TYPE_SUBSCRIBE = "subscribe"
const val VAL_TYPE_SUBSCRIBED = "subscribed"
const val VAL_TYPE_UNSUBSCRIBED = "unsubscribed"

const val PRESENCE_CHAT = "chat"
const val PRESENCE_DND = "dnd"
const val PRESENCE_AWAY = "away"
const val PRESENCE_UNA = "unavailable"
const val PRESENCE_XA = "xa"

class Presence(
        val from: String?,
        val to: String?,
        val type: String?
) : Stanza {

    var show: Tag? = null

    fun withShow(show: Tag?): Presence {
        this.show = show
        return this
    }

    fun getStatus() = when (show?.data) {
        Status.CHAT.state -> Status.CHAT
        Status.AWAY.state -> Status.AWAY
        else -> Status.UNKNOWN
    }

    fun getSubState() = when (type) {
        SubState.REQUEST.type -> SubState.REQUEST
        SubState.ACK.type -> SubState.ACK
        else -> SubState.UNKNOWN
    }

    override fun xml() =
            "<$Q_NAME_PRESENCE" +
                    (if (from != null) " $ATTR_FROM='$from'" else "") +
                    (if (to != null) " $ATTR_TO='$to'" else "") +
                    (if (type != null) " $ATTR_TYPE='$type'" else "") +
                    ">" +
                    (if (show != null) "\n${show?.xml()}\n" else "") +
                    "</$Q_NAME_PRESENCE>"

    override fun toString() =
            "Presence: type=$type, to=$to, from=$from\n" +
                    "$show\n"

    companion object {
        @JvmStatic
        fun status(status: Status) = Presence(
                null,
                null,
                null
        ).withShow(Tag(Q_NAME_SHOW).withData(status.state)).xml()

        @JvmStatic
        fun subscribe(to: String?) = Presence(
                null,
                to,
                VAL_TYPE_SUBSCRIBE
        ).xml()

        @JvmStatic
        fun ackSub(to: String?, allow: Boolean) = Presence(
                null,
                to,
                if (allow) VAL_TYPE_SUBSCRIBED else VAL_TYPE_UNSUBSCRIBED
        ).xml()

    }

}