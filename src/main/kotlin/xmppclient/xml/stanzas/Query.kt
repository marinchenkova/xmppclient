package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.ATTR_NS
import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.Tag

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_QUERY = "query"

const val VAL_NS_REGISTER = "jabber:iq:register"

const val TAG_USERNAME = "username"
const val TAG_PASSWORD = "password"
const val TAG_EMAIL = "email"

class Query(val xmlns: String?) : Stanza {

    val tags = HashMap<String, Stanza>()
    val items = ArrayList<Item>()

    override fun xml() =
        "<$Q_NAME_QUERY $ATTR_NS='$xmlns'>\n" +
                (if (tags.size > 0) "${tags.values.joinToString("\n") { it.xml() }}\n" else "") +
                (if (items.size > 0) "${items.joinToString("\n") { it.xml() }}\n" else "") +
                "</$Q_NAME_QUERY>"

    override fun toString() = "Query: xmlns=$xmlns\n" +
            tags.values.joinToString { it.toString() } + "\n" +
            items.joinToString { it.toString() }


}