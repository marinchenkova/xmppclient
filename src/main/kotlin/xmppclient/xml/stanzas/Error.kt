package xmppclient.xml.stanzas

import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.Tag

/**
 * @author Marinchenko V. A.
 */

const val Q_NAME_ERROR = "error"
const val Q_NAME_ERROR_BAD_REQUEST = "bad-request"
const val Q_NAME_ERROR_CONFLICT = "conflict"
const val Q_NAME_ERROR_FNI = "feature-not-implemented"
const val Q_NAME_ERROR_FORBIDDEN = "forbidden"
const val Q_NAME_ERROR_GONE = "gone"
const val Q_NAME_ERROR_ISE = "internal-server-error"
const val Q_NAME_ERROR_INF = "item-not-found"
const val Q_NAME_ERROR_JM = "jid-malformed"
const val Q_NAME_ERROR_N_ACC = "not-acceptable"
const val Q_NAME_ERROR_N_ALL = "not-allowed"
const val Q_NAME_ERROR_N_AUTH = "not-authorized"
const val Q_NAME_ERROR_PAYMENT = "payment-required"
const val Q_NAME_ERROR_REDIRECT = "redirect"
const val Q_NAME_ERROR_RSNF = "remote-server-not-found"
const val Q_NAME_ERROR_RST = "remote-server-timeout"
const val Q_NAME_ERROR_RES_CON = "resource-constraint"
const val Q_NAME_ERROR_SERVICE_UN = "service-unavailable"
const val Q_NAME_ERROR_SUB_REQ = "subscription-required"
const val Q_NAME_ERROR_UNDEF_COND = "undefined-condition"
const val Q_NAME_ERROR_UNEXP_REQ = "unexpected-request"

const val ATTR_CODE = "code"

class Error(val code: String?, val type: String?) : Stanza {

    var data: String? = null
    var what: String? = null

    fun withWhat(what: String?): Error {
        this.what = what
        return this
    }

    fun withData(data: String?): Error {
        this.data = data
        return this
    }

    override fun xml() =
            "<$Q_NAME_ERROR " +
                    (if (code != null) "$ATTR_CODE='$code' " else "") +
                    "${if (type != null) "$ATTR_TYPE='$type'" else ""}>" +
                    (if (data != null) "$\ndata\n" else "") +
                    "</$Q_NAME_ERROR>"

    override fun toString() = "Error, code=$code, type=$type: $data"

}