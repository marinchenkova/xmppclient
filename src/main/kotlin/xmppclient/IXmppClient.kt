package xmppclient

import xmppclient.entity.Status

/**
 * @author Marinchenko V. A.
 */
interface IXmppClient {

    fun connect(server: String, port: Int)

    fun disconnect()

    fun isConnected(): Boolean

    fun login(login: String, password: String, base64data: String)

    fun logout()

    fun isLogged(): Boolean

    fun handshake()

    fun updateStatus(status: Status)

    fun writeTo(jid: String, msg: String)

    fun ackSubscribeFrom(jid: String, allow: Boolean)

    fun removeContact(jid: String)

    fun subscribeTo(jid: String, group: String)

    fun requestContacts()

}