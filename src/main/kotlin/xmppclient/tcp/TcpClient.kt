package xmppclient.tcp

import xmppclient.util.MessageBuffer
import xmppclient.util.ResponseListener
import java.io.PrintWriter
import java.net.InetAddress
import java.net.Socket
import kotlin.concurrent.thread

/**
 * @author Marinchenko V. A.
 */
class TcpClient : ITcpClient {

    @Volatile private var received = 0
    @Volatile private var last = 0
    @Volatile private var async: Boolean = true
    private var socket: Socket? = null
    var inListener: ResponseListener? = null

    private fun init() = thread(true) {
        while (receive());
    }

    override fun connect(address: InetAddress, port: Int) {
        if (socket == null) try {
            socket = Socket(address, port)
        } catch (e: Exception) {
        }
        init()
    }

    fun connect(address: String, port: Int) {
        connect(InetAddress.getByName(address), port)
    }

    override fun disconnect() {
        socket?.close()
        socket = null
    }

    override fun isConnected(): Boolean {
        val connected = socket?.isConnected
        return !(connected == null || !connected)
    }

    override fun send(msg: String) {
        PrintWriter(socket?.getOutputStream(), true).apply {
            println(msg)
            flush()
        }
    }

    fun asyncReceive(enable: Boolean) {
        async = enable
    }

    override fun recvString(): String? {
        val bufSize = socket?.receiveBufferSize ?: 0
        val buf = ByteArray(bufSize)

        val read = try {
            socket?.getInputStream()?.read(buf) ?: -1
        } catch (e: Exception) {
            -1
        }
        if (read <= 0) return null

        return String(buf, 0, read)
    }

    fun waitForResponse(num: Int = 1) {
        last = received
        while (received - last < num);
        last = 0
        received = 0
    }

    private fun receive() = synchronized(this) {
        if (async) {
            val msg = recvString() ?: return@synchronized false
            inListener?.onReceive(msg)
            received++
        }
        true
    }


}