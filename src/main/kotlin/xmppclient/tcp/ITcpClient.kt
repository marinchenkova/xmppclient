package xmppclient.tcp

import java.net.InetAddress

/**
 * @author Marinchenko V. A.
 */
interface ITcpClient {

    fun connect(address: InetAddress, port: Int)

    fun disconnect()

    fun isConnected(): Boolean

    fun send(msg: String)

    fun recvString(): String?

}