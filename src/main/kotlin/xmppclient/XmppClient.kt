package xmppclient

import xmppclient.entity.Contact
import xmppclient.entity.Status
import xmppclient.entity.SubState
import xmppclient.entity.SubType
import xmppclient.tcp.TcpClient
import xmppclient.util.ResponseListener
import xmppclient.util.XmppListener
import xmppclient.xml.parser.Operation
import xmppclient.xml.parser.XmlParser
import xmppclient.xml.parser.extract
import xmppclient.xml.stanzas.*
import xmppclient.xml.stanzas.base.Stanza
import xmppclient.xml.stanzas.base.VAL_XML_LANG_EN
import xmppclient.xml.stanzas.stream.Auth
import xmppclient.xml.stanzas.stream.AuthMechanism
import xmppclient.xml.stanzas.stream.Stream


const val ERROR_CONN_REFUSED = "Error: connection refused"
const val FRIEND_GROUP_DEFAULT = "Auto"

/**
 * @author Marinchenko V. A.
 */
class XmppClient: IXmppClient {

    // Attributes
    private val version = "1.0"
    private var xmlLang: String? = VAL_XML_LANG_EN

    // Session parameters
    var server: String = ""
        private set
    private var login: String = ""
    private var password: String = ""

    var userid: String = ""
        private set

    var fullJid: String? = ""
        private set

    private var stream: Stream? = null

    private val tcpClient = TcpClient()
    private val parser = XmlParser()
    private var isLogged: Boolean = false

    var listener: XmppListener? = null

    val contacts = HashMap<String?, Contact>()

    init {
        tcpClient.inListener = object : ResponseListener {
            override fun onReceive(msg: String?) {
                receive(msg)
            }
        }
    }

    private fun receive(msg: String?) {
        if (msg == null || msg.isEmpty() || msg.isBlank()) return

        val response = parser.getResponse(msg)
        printResponse(msg, response.operation.type, response.map)

        when (response.operation) {
            Operation.INIT_STREAM -> {
                stream = response.map.extract()
                if (stream?.id == null) {
                    disconnect()
                }
                xmlLang = stream?.xmlLang
            }

            Operation.STREAM_FEATURES -> {
                stream?.features = response.map.extract<Stream>()?.features
                if (stream?.id == null) {
                    disconnect()
                }
            }

            Operation.AUTH -> {
                isLogged = msg.contains(RESULT_SUCCESS)
            }

            Operation.BIND -> {
                val iq = response.map.extract<Iq>()
                fullJid = iq?.bind?.getJid()
            }

            Operation.ROSTER_SET -> {
                val iq = response.map.extract<Iq>()
                val items = iq?.query?.items
                if (items != null && items.size > 0) items.forEach {
                    contacts[it.jid]?.sub = it.getSubType()
                    if (it.getSubType() == SubType.REMOVE) contacts.remove(it.jid)
                    if (it.ask != null) listener?.onSubscribeAck(it.jid)
                }
                val request = Iq.ackPush(iq?.id)
                printRequest(request, "Ack roster push")
                tcpClient.send(request)
            }

            Operation.ROSTER_RESULT -> {
                val iq = response.map.extract<Iq>()
                val items = iq?.query?.items
                if (items != null) {
                    contacts.putAll(items.toContacts())
                }
                listener?.onRosterChanged(contacts)
            }

            Operation.PRESENCE -> response.map.forEach {
                val presence = it as Presence

                if (presence.type == null) {
                    val status = presence.getStatus()
                    contacts[presence.from]?.status = status
                    if (presence.from != userid) listener?.onFriendStatusChanged(presence.from, status)
                }

                if (presence.type == Status.UNAVAILABLE.state) {
                    listener?.onFriendStatusChanged(presence.from, Status.UNAVAILABLE)
                }

                when (presence.getSubState()) {
                    SubState.ACK -> {}
                    SubState.REQUEST -> listener?.onSubscribeRequest(presence.from)
                    else -> {}
                }
            }

            Operation.MESSAGE -> {
                val message = response.map.extract<Message>()
                if (message?.getText() != null) {
                    listener?.onMsgReceived(message.from, message.getText())
                }
            }

            else -> {}
        }
    }

    override fun connect(server: String, port: Int) {
        this.server = server
        tcpClient.connect(server, port)
        initStream()
    }

    override fun disconnect() {
        tcpClient.disconnect()
        println("Disconnected")
        server = ""
    }

    override fun isConnected() = tcpClient.isConnected()

    override fun login(login: String, password: String, base64data: String) {
        this.login = login
        this.password = password
        this.userid = "$login@$server"

        auth(base64data)
    }

    override fun logout() {
        login = ""
        password = ""
        userid = ""
        updateStatus(Status.AWAY)
        disconnect()
    }

    override fun isLogged() = isLogged

    override fun handshake() {
        initStream()
        bindRes()
    }

    private fun bindRes() {
        val request = Iq.bind()
        printRequest(request, "Bind resource")
        tcpClient.send(request)
        tcpClient.waitForResponse()
    }

    override fun updateStatus(status: Status) {
        val request = Presence.status(status)
        printRequest(request, "Update status")
        tcpClient.send(request)
    }

    override fun writeTo(jid: String, msg: String) {
        val request = Message.write(jid, fullJid, msg)
        printRequest(request, "Write message")
        tcpClient.send(request)
    }

    override fun ackSubscribeFrom(jid: String, allow: Boolean) {
        val request1 = Presence.ackSub(jid, allow)
        printRequest(request1, "Ack subscription")
        tcpClient.send(request1)

        if (allow) {
            val request2 = Presence.subscribe(jid)
            printRequest(request1, "Ack subscription and subscribe")
            tcpClient.send(request2)
            subscribeTo(jid, FRIEND_GROUP_DEFAULT)
        }
    }

    override fun removeContact(jid: String) {
        val request = Iq.rosterRemove(fullJid, jid)
        printRequest(request, "Remove contact")
        tcpClient.send(request)
        requestContacts()
    }

    override fun subscribeTo(jid: String, group: String) {
        val request1 = Iq.rosterAdd(jid, group)
        val request2 = Presence.subscribe(jid)
        printRequest(request1, "Subscribe, info for server")
        printRequest(request2, "Subscribe, info for client")
        tcpClient.send(request1)
        tcpClient.send(request2)
        requestContacts()
    }

    override fun requestContacts() {
        val request = Iq.rosterGet(fullJid)
        printRequest(request, "Request contacts")
        tcpClient.send(request)
        tcpClient.waitForResponse()
    }

    private fun initStream() {
        val request = Stream.to(server, null, version, xmlLang)
        printRequest(request, "Init stream")
        tcpClient.send(request)
        tcpClient.waitForResponse(1)
    }

    private fun auth(base64data: String) {
        authPlain(base64data)
    }

    private fun authPlain(base64data: String) {
        val xml = Auth(AuthMechanism.PLAIN, base64data).xml()
        printRequest(xml, "Auth")
        tcpClient.send(xml)
        tcpClient.waitForResponse()
    }

    private fun printRequest(request: String?, type: String? = null, stanzas: ArrayList<Stanza?>? = null) {
        printStanza("client", request, stanzas, type)
    }

    private fun printResponse(response: String?, type: String? = null, stanzas: ArrayList<Stanza?>? = null) {
        printStanza("server", response, stanzas, type)
    }

    private fun printStanza(source: String, msg: String?, stanzas: ArrayList<Stanza?>?, type: String?) {
        println("[ $source ]:")

        if (type != null) println("Type: $type")

        println("XML: ")
        println(msg)
        /*
        if (stanzas != null) {
            println("Stanzas: [")
            stanzas.forEach { _, u -> println("${u.toString()}\n") }
            println("]\n")
        }
        */

        println()
    }

    companion object {
        val XMPP_CLIENT_PORT_1 = 5222
        val XMPP_CLIENT_PORT_2 = 5269
    }
}