package xmppclient.tcp

import org.junit.Test
import xmppclient.util.ResponseListener


/**
 * @author Marinchenko V. A.
 */
class TcpAppTest {


    @Test
    fun test() {
        val client = TcpClient()
        client.inListener = object : ResponseListener {
            override fun onReceive(msg: String?) {
                println("LISTENER: $msg")
            }
        }
        client.connect("127.0.0.1", 7500)
        client.send("request1")

        Thread.sleep(3000)

        client.disconnect()
    }
}
