package xmppclient.xmpp

import org.junit.Test
import xmppclient.FRIEND_GROUP_DEFAULT
import xmppclient.XmppClient
import xmppclient.entity.Contact
import xmppclient.entity.Status
import xmppclient.util.XmppListener
import java.util.*
import kotlin.collections.HashMap

/**
 * @author Marinchenko V. A.
 */
class XmppClientTest {

    private val server1 = "jabber.ru"
    private val server2 = "jabberon.ru"
    private val port = 5222
    private val login1 = "mvatesttest"
    private val login2 = "mvatesttest2"
    private val pass = "mvatesttest"
    private val client = XmppClient()

    @Test
    fun xmppStreamTest() {
        client.connect(server1, port)
        client.listener = object : XmppListener {
            override fun onRosterChanged(contacts: HashMap<String?, Contact>) {

            }

            override fun onFriendStatusChanged(jid: String?, status: Status) {
                println("FRIEND $jid STATUS ${status.state}")
            }

            override fun onSubscribeRequest(jid: String?) {
                println("SUB REQUEST FROM $jid")
            }

            override fun onSubscribeAck(jid: String?) {
                println("SUB ACK FROM $jid")
            }

            override fun onMsgReceived(jid: String?, text: String?) {
                println("MESSAGE FROM $jid: $text")
            }
        }

        if (!client.isConnected()) {
            println("Connection refused")
            return
        }




        client.login(login2, pass, encodeAuth("$login2@$server1", login2, pass))

        if (!client.isLogged()) {
            println("Login failed")
            return
        }

        client.handshake()


        client.writeTo("$login1@${client.server}", "Hello from 2!")

        //client.subscribeTo("$login2@${client.server}", FRIEND_GROUP_DEFAULT)

        //client.ackSubscribeFrom("$login2@${client.server}", true)

        //client.removeContact("$login2@${client.server}")


        //client.requestContacts()

        Thread.sleep(1000)

        client.disconnect()
    }

    fun encodeAuth(userid: String, login: String, password: String): String {
        val str = "$userid\u0000$login\u0000$password"
        return String(Base64.getEncoder().encode(str.toByteArray()))
    }

}